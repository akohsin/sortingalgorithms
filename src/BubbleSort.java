public class BubbleSort {

    public static void sort(int[] array) {
        int counter = 0;
        int n = array.length;
        // for j in range(0,n):
        for (int j = 0; j < n; j++) {
            printArray(array);
            // for i in range(0,n-1):
            for (int i = 0; i < n - 1; i++) {
                //if(A[i] > A[i+1]):
                counter++;
                if (array[i] > array[i + 1]) {
                    //A[i],A[i+1] = A[i+1],A[i]
                    // zmiana wartosci
                    int tmp = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = tmp;
                }
            }
        }
        System.out.println("Licznik: " + counter);
    }

    public static void printArray(int[] array) {
        for (int value : array) {
            System.out.print(value);
            System.out.print(", ");
        }
        System.out.println();
    }
}
