import java.util.Arrays;

public class InsertionSort {
    public static void main(String[] args) {
        int array[] = new int[]{214, 234, 432, 32, 234, 23, 434, 3, 3, 3, 3, 4, 34, 234, 324, 23, 532, 5, 325, 35, 354,
                324, 3223, 2, 2, 2, 432, 423};
        sort(array);

    }


    public static void sort(int[] array) {
        int n = array.length;
        int counter = 0;
        for (int i = 1; i < n; i++) {
            int wartosc = array[i];
            int j = i;

            while (j > 0 && wartosc < array[j - 1]) {
                counter++;
                array[j] = array[j - 1];
                j = j - 1;
            }
            array[j] = wartosc;
            BubbleSort.printArray(array);
        }

        System.out.println("Licznik: " + counter);
    }
}

