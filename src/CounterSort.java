
    public class CounterSort {

        // zalozenie - liczba z zakresu do 16
        public static void sort(int[] array) {
            // N razy - iteracja przez N elementów
            int[] ilosciWystapien = new int[16]; // 0-15
            for (int i = 0; i < array.length; i++) {
                int wartosc = array[i]; // wartosc liczby
                ilosciWystapien[wartosc] = ilosciWystapien[wartosc] + 1; // inkrementuje
            }

            int licznik = 0;
            for (int i = 0; i < 16; i++) {  // przechodzimy przez kubelki
                for (int j = 0; j < ilosciWystapien[i]; j++) { // ilosc wystapien razy
                    // przepisujemy wartosc do tablicy
                    array[licznik++] = i;
                }
            }
        }
    }
